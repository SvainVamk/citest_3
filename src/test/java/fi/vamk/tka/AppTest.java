package fi.vamk.tka;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }

    @Test
    public void isMonday() {
        assertEquals("Hello Monday World!", App.helloWeekday("16/03/2020"));
    }

    @Test
        public void isTuesday() {
            assertEquals("Hello Tuesday World!", App.helloWeekday("17/03/2020"));
        }

    @Test
        public void isWorld() {
            assertEquals("Hello World!", App.helloWeekday(""));
        }

    @Test
    public void isNull() {
        assertEquals("Hello World!", App.helloWeekday(null));
    }
}
